# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Jorge Rincón Moráis
* Titulación: Ingeniería en Sistemas de Telecomunicación + Administración y Dirección de Empresas
* Cuenta en laboratorios: jrincon
* Cuenta URJC: j.rincon.2019@alumnos.urjc.es
* Video básico (url): https://youtu.be/9Ghhq6tFNNs
* Video parte opcional (url): https://youtu.be/jGQsn8ZwesI
* Despliegue (url): http://jrincon.pythonanywhere.com/
* Contraseñas: No son necesarias.
* Cuenta Admin Site: admin/admin

## Resumen parte obligatoria
* Página Principal: Incluye todos los comentarios que se han incluido en las cámaras con el nombre de la cámara, el ID de la cámara, el nombre del comentador, la fecha y el texto del comentario.
* Página de las Cámaras: Incluye la imagen aleatoria de una de las fuentes disponibles, la opción de descargar los listados de cámaras y la lista con las camaras activas, cada una con su información.
* Página de la Camara: Incluye la información de la cámara, su imagen y debajo los comentarios que tiene esa misma cámara.
* Página de la Camara Dinámica: Incluye la información de la cámara, pero a diferencia de la anterior su imagen y los comentarios se recargan automáticamente cada 30 segundos gracias al HTMX.
* Página de Comentario: Incluye la información de la cámara, junto con la fecha en el momento que se va a poner el comentario y la imagen de la cámara en ese momento. Debajo aparece un formulario para poner un texto y al rellenarlo se redirige a la página de la Cámara Dinámica donde se podrá apreciar el comentario nuevo.
* Página de Configuración: Permite cambiar el nombre del comentador, formato de la letra de la página y el tamaño de la letra según las necesidades del cliente.
* Página de Ayuda: Incluye la información del autor de la práctica, junto con una breve explicación de la misma.
* Página de Admin: Permite administrar los datos de la base de datos de la aplicación en Django.


## Lista partes opcionales

* Favicon: inclusión de un favicon del sitio.
* Bootstrap: inclusión de varios elementos de Bootstrap, para que aparezca la barra de navegación si la pestaña se hace pequeña, en la página de ayuda un elemento Carousel (con imágenes relacionadas con la universidad y la asignatura) y clases adicionales para mejorar la apariencia visual de mi aplicación.
* Tests: he creado más de un test extremo a extremo por recurso y, además he creado tests unitarios para comprobar el funcionamiento de algunas funciones de mi código.
* Terminar sesión: en la página de configuración he incluido un botón que permite que, al usarlo, redirija al usuario a la página principal con la configuración por defecto, que en mi caso es nombre 'Anónimo', formato de letra 'Calibrí' y tipo de letra 'Medium'.


## PythonAnyWhere
Al subir la práctica a PAW, la función descargar_imagen del views.py no descarga la imagen correctamente al no estar en la Whitelist de PAW, me pinta un error en el apartado de server.log: "HTTP Error 403: Forbidden".
