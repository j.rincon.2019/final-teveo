from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from .models import Camara


class Listado2xmlParser:
    def __init__(self, xml):
        self.parse = make_parser()
        self.handler = Listado2()
        self.parse.setContentHandler(self.handler)
        self.parse.parse(xml)


class Listado2(ContentHandler):
    def __init__(self):
        self.inCamera = False
        self.inContent = False
        self.content = ""
        self.id = ""
        self.font = ""
        self.ubi = ""
        self.longitud = ""
        self.latitud = ""

    def startElement(self, name, attrs):
        if name == "cam":
            self.inCamera = True
            self.id = f"LIS2-{attrs.get('id')}"
        elif self.inCamera:
            if name in ["url", "info", "latitude", "longitude"]:
                self.inContent = True

    def endElement(self, name):
        if name == "cam":
            self.inCamera = False
            camera = Camara(
                id=self.id,
                fuente=self.font,
                ubicacion=self.ubi,
                longitud=self.longitud,
                latitud=self.latitud
            )
            camera.save()
        elif self.inCamera:
            if name == "url":
                self.font = self.content
            elif name == "info":
                self.ubi = self.content
            elif name == "latitude":
                self.latitud = self.content
            elif name == "longitude":
                self.longitud = self.content

            self.inContent = False
            self.content = ""

    def characters(self, chars):
        if self.inContent:
            self.content += chars
