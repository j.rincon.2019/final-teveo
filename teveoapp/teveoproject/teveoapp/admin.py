from django.contrib import admin
from .models import Usuario, Camara, Comentario
# Register your models here.

admin.site.register(Usuario)
admin.site.register(Camara)
admin.site.register(Comentario)
