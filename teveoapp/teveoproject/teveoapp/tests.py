from django.test import TestCase, Client
from .models import Usuario, Camara, Comentario
from .views import descargar_imagen
from django.utils import timezone

# Create your tests here.


# TESTS DE RECURSO Y UNITARIOS


class IndexTest(TestCase):
    def test_index(self):
        client = Client()
        response = client.get("/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "teveoapp/index.html")

    def test_html(self):
        client = Client()
        response = client.get("/")
        content = response.content.decode('utf-8')
        self.assertIn("<p><strong>Total de Cámaras:</strong>", content)
        # Si primer string (<h5>) está dentro del segundo string (content)


class CamarasTest(TestCase):
    def test_camaras(self):
        client = Client()
        response = client.get("/camaras/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "teveoapp/camaras.html")


class CamaraTest(TestCase):
    def test_camara(self):
        client = Client()
        camara = Camara.objects.create(id="A", ubicacion="Prueba Test Location", longitud="1.0", latitud="2.0",
                                       fuente="http://infocar.dgt.es/etraffic/data/camaras/3.jpg")
        response = client.get(f"/camara/{camara.id}/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "teveoapp/camara.html")


class CamaraDinamicaTest(TestCase):
    def test_camaradyn(self):
        client = Client()
        camara = Camara.objects.create(id="B", ubicacion="Prueba Test Location", longitud="3.0", latitud="4.0",
                                       fuente="http://infocar.dgt.es/etraffic/data/camaras/4.jpg")
        response = client.get(f"/camaradyn/{camara.id}/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "teveoapp/camaradyn.html")


class ComentarioDinamicoTest(TestCase):
    def test_comentdyn(self):
        client = Client()
        camara = Camara.objects.create(id="A", ubicacion="Prueba Test Location", longitud="1.0", latitud="2.0",
                                       fuente="http://infocar.dgt.es/etraffic/data/camaras/3.jpg")
        response = client.get(f"/comment_embedded/{camara.id}/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "teveoapp/comentariosdyn.html")


class ComentarioTest(TestCase):
    def test_coment(self):
        client = Client()
        camara = Camara.objects.create(id="B", ubicacion="Prueba Test Location", longitud="3.0", latitud="4.0",
                                       fuente="http://infocar.dgt.es/etraffic/data/camaras/4.jpg")
        response = client.get(f"/comentario/{camara.id}/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "teveoapp/comentario.html")


class ConfiguracionTest(TestCase):
    def test_config(self):
        client = Client()
        response = client.get("/configuracion/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "teveoapp/configuracion.html")

    def test_user(self):
        client = Client()

        # Establecer una sesión con una session_key
        session = client.session
        session.save()

        # Hacer una solicitud POST para crear un usuario con datos
        response = client.post("/configuracion/", {
            'nombre': 'Kylian',
            'formato': 'Cursive',
            'tipo': 'Small'
        })

        # Verificar que la respuesta es un una redirección a la configuración
        self.assertEqual(response.status_code, 302)

        # Obtener el usuario de la base de datos
        current_session = session.session_key
        usuario, creado = Usuario.objects.get_or_create(id=current_session)

        # Verificar que los datos del usuario son correctos
        self.assertEqual(usuario.nombre, 'Kylian')
        self.assertEqual(usuario.formato, 'Cursive')
        self.assertEqual(usuario.tipo, 'Small')

        # Hacer una solicitud GET para verificar que los datos se muestran correctamente en la plantilla
        response = client.get("/configuracion/")
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Kylian')
        self.assertContains(response, 'Cursive')
        self.assertContains(response, 'Small')


class AyudaTest(TestCase):
    def test_ayuda(self):
        client = Client()
        response = client.get("/ayuda/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "teveoapp/ayuda.html")

    def test_plantilla(self):
        client = Client()
        response = client.get("/ayuda/")
        content = response.content.decode('utf-8')
        self.assertIn("<p><strong>Nombre:</strong> Jorge Rincón Moráis.</p>", content)


class JsonTest(TestCase):
    def test_json(self):
        client = Client()
        camara = Camara.objects.create(id="A", ubicacion="Test Location", longitud="1.0", latitud="2.0",
                                       fuente="http://infocar.dgt.es/etraffic/data/camaras/3.jpg")
        response = client.get(f"/json/{camara.id}/")
        self.assertEqual(response.status_code, 200)


class UsuarioModelTest(TestCase):
    def test_creacion_usuario(self):
        usuario = Usuario.objects.create(nombre='Pepito', formato='Courier-New', tipo='Large')
        self.assertEqual(usuario.nombre, 'Pepito')
        self.assertEqual(usuario.formato, 'Courier-New')
        self.assertEqual(usuario.tipo, 'Large')


class ImagenTest(TestCase):
    def test_descargar_imagen(self):
        camara = Camara.objects.create(id="C", ubicacion="Concha Espina", longitud="1.0", latitud="2.0",
                                       fuente="http://infocar.dgt.es/etraffic/data/camaras/4.jpg")
        response = descargar_imagen(camara.id)  # Llama a descargar imagen con la camara creada
        self.assertIsNotNone(response)  # Verifica que la función no devuelve none
        self.assertIsInstance(response, bytes)  # Verifica que la respuesta sean bytes, sino falla

    def test_image_embedded(self):
        client = Client()
        camara = Camara.objects.create(id="D", ubicacion="Malaga", longitud="5.0", latitud="6.0",
                                       fuente="http://infocar.dgt.es/etraffic/data/camaras/4.jpg")
        response = client.get(f"/image_embedded/{camara.id}/")
        self.assertEqual(response.status_code, 200)
        self.assertIn('<img src="data:image/jpeg;base64,', response.content.decode('utf-8'))


class ComentarioTestCase(TestCase):
    # Método setUp para que se ejecute primero el setUp antes que test_comentario_creado
    # El test_comentario_creado accede a self.comentario y si se ejecuta antes no funciona, esto se soluciona con setUp
    def setUp(self):
        self.camara = Camara.objects.create(
            id="Camara Murcia",
            ubicacion="Avenida de Murcia",
            longitud="1.5",
            latitud="2.5",
            fuente="http://fuentedeprueba.com"
        )

        self.comentario = Comentario.objects.create(
            camara=self.camara,
            autor_nombre="Pablo",
            texto="Este es un comentario de prueba",
            fecha=timezone.now(),
            imagen="data:image/jpeg;base64,6p/4Aasfoga..."
        )

    def test_comentario_creado(self):
        """
        Prueba que el comentario se ha creado correctamente.
        """
        self.assertEqual(self.comentario.autor_nombre, "Pablo")
        self.assertEqual(self.comentario.texto, "Este es un comentario de prueba")
        self.assertIsNotNone(self.comentario.fecha)  # Campo fecha no sea nulo
        self.assertEqual(self.comentario.imagen, "data:image/jpeg;base64,6p/4Aasfoga...")
