from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("camaras/", views.camaras, name="camaras"),
    path("camara/<str:id>/", views.camara, name="camara"),
    path("camaradyn/<str:id>/", views.camaradyn, name="camaradyn"),
    path('comentario/<str:id>/', views.comentario, name='comentario'),
    path("image_embedded/<str:camara_id>/", views.image_embedded, name="image_embedded"),
    path('comment_embedded/<str:camara_id>/', views.comment_embedded, name='comment_embedded'),
    path("json/<str:id>/", views.json, name="json"),
    path("configuracion/", views.configuracion, name="configuracion"),
    path("ayuda/", views.ayuda, name="ayuda"),
    path("salir/", views.salir, name="salir"),
]
