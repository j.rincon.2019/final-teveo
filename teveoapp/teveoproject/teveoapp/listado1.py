from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from .models import Camara


class Listado1xmlParser:
    def __init__(self, xml):
        self.parse = make_parser()
        self.handler = Listado1()
        self.parse.setContentHandler(self.handler)
        self.parse.parse(xml)


class Listado1(ContentHandler):
    def __init__(self):
        self.inCamera = False
        self.inContent = False
        self.content = ""
        self.id = ""
        self.font = ""
        self.ubi = ""
        self.coords = ""
        self.longitud = ""
        self.latitud = ""

    def startElement(self, name, attrs):
        if name == "camara":
            self.inCamera = True
        elif self.inCamera:
            if name in ["id", "src", "lugar", "coordenadas"]:
                self.inContent = True

    def endElement(self, name):
        if name == "camara":
            self.inCamera = False
            cam = Camara(
                id=self.id,
                fuente=self.font,
                ubicacion=self.ubi,
                longitud=self.longitud,
                latitud=self.latitud
            )
            cam.save()

        elif self.inCamera:
            if name == "id":
                self.id = f"LIS1-{self.content}"
            elif name == "src":
                self.font = self.content
            elif name == "lugar":
                self.ubi = self.content
            elif name == "coordenadas":
                coordenadas = self.content.split(",")
                self.longitud = coordenadas[1]
                self.latitud = coordenadas[0]

            self.inContent = False
            self.content = ""

    def characters(self, chars):
        if self.inContent:
            self.content += chars
