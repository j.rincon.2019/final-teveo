from django.shortcuts import render, redirect, get_object_or_404
from .models import Usuario, Camara, Comentario
from django.http import HttpResponse
import urllib.request
from .listado1 import Listado1xmlParser
from .listado2 import Listado2xmlParser
import random
from django.utils import timezone
import base64
from django.http import JsonResponse, HttpResponseNotFound

# Create your views here.

# source django-venv/bin/activate
# python3 manage.py makemigrations
# python3 manage.py migrate
# python3 manage.py createsuperuser
# python3 manage.py runserver 8080
# ./manage.py test


def index(request):
    """
    Manejador de página principal
    """
    if not request.session.session_key:
        request.session.create()

    session_key = request.session.session_key
    configuracion, created = Usuario.objects.get_or_create(
        id=session_key,
        defaults={'nombre': 'Anonimo', 'formato': 'Calibri', 'tipo': 'Medium'}
    )

    camaras = Camara.objects.all()

    comentarios = Comentario.get_ordered_comments()

    n_camaras = Camara.get_ordered_by_num_comments().count()
    n_coments = comentarios.count()

    context = {
        "camaras": camaras,  # Todas las cámaras
        "comentarios": comentarios,  # Todos los comentarios ordenados por fecha descendente
        "n_camaras": n_camaras,
        "n_coments": n_coments,
        "usuario": configuracion
    }

    return render(request, "teveoapp/index.html", context)


def obtener_lista1():
    url = "https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado1.xml"
    xml = urllib.request.urlopen(url)
    Listado1xmlParser(xml)


def obtener_lista2():
    url = "https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado2.xml"
    xml = urllib.request.urlopen(url)
    Listado2xmlParser(xml)


def camaras(request):
    """
    Manejador de vista camaras
    """
    if not request.session.session_key:
        request.session.create()

    session_key = request.session.session_key
    configuracion, created = Usuario.objects.get_or_create(
        id=session_key,
        defaults={'nombre': 'Anonimo', 'formato': 'Calibri', 'tipo': 'Medium'}
    )

    if request.method == "POST":
        if "lista1" in request.POST:
            obtener_lista1()
        elif "lista2" in request.POST:
            obtener_lista2()
        return redirect("/camaras/")

    elif request.method == "GET":
        camaras = Camara.objects.all()
        camaras_ordenadas = Camara.get_ordered_by_num_comments()
        comentarios = Comentario.get_ordered_comments()
        n_camaras = Camara.get_ordered_by_num_comments().count()
        n_coments = comentarios.count()

        imagen_random = random.choice(camaras) if camaras else None

        context = {
            "camaras": camaras_ordenadas,
            "n_camaras": n_camaras,
            "n_coments": n_coments,
            "imagen_random": imagen_random,
            "configuracion": configuracion,
            "usuario": configuracion
        }
        return render(request, "teveoapp/camaras.html", context)


def obtener_datos_camara(request, id):
    """
    Función auxiliar para obtener los datos de una cámara.
    """
    if not request.session.session_key:
        return None, None, None, None, None

    session_key = request.session.session_key
    configuracion, created = Usuario.objects.get_or_create(
        id=session_key,
        defaults={'nombre': 'Anonimo', 'formato': 'Calibri', 'tipo': 'Medium'}
    )

    try:
        comentarios = Comentario.get_ordered_comments()

        n_camaras = Camara.get_ordered_by_num_comments().count()
        n_coments = comentarios.count()

        camara = Camara.objects.get(id=id)
        comentarios = Comentario.get_ordered_comments().filter(camara=camara)
        return n_camaras, n_coments, camara, comentarios, configuracion
    except Camara.DoesNotExist:
        return None, None, None, None, None


def camara(request, id):
    """
    Manejador de vista de una cámara
    """
    if request.method == "GET":
        if not request.session.session_key:
            request.session.create()

        n_camaras, n_coments, camara, comentarios, configuracion = obtener_datos_camara(request, id)
        if camara:
            return render(request, "teveoapp/camara.html", {
                "camara": camara,
                "comentarios": comentarios,
                "n_camaras": n_camaras,
                "n_coments": n_coments,
                "configuracion": configuracion,
                "usuario": configuracion
            })
        else:
            return HttpResponse("Error, no existe la cámara que busca", status=404)


def camaradyn(request, id):
    """
    Manejador de vista de una cámara dinámica
    """
    if request.method == "GET":
        if not request.session.session_key:
            request.session.create()

        n_camaras, n_coments, camara, comentarios, configuracion = obtener_datos_camara(request, id)
        if camara:
            return render(request, "teveoapp/camaradyn.html", {
                "camara": camara,
                "comentarios": comentarios,
                "n_camaras": n_camaras,
                "n_coments": n_coments,
                "configuracion": configuracion,
                "usuario": configuracion
            })
        else:
            return HttpResponse("Error, no existe la cámara que busca", status=404)


def capturar_imagen_de_camara(camara):  # Para guardar la imagen con el comentario
    try:
        # Descargar la imagen de la cámara en la que se va a comentar
        image = descargar_imagen(camara.id)
        if image is not None:
            # Convertir la imagen a base64
            image_base64 = base64.b64encode(image).decode('utf-8')
            return image_base64
    except Exception as e:
        print(f"Error al capturar imagen de la cámara: {e}")
    return None


def comentario(request, id):

    session_key = request.session.session_key
    configuracion = Usuario.objects.filter(id=session_key).first()

    camara = get_object_or_404(Camara, id=id)
    comentarios = Comentario.get_ordered_comments()

    n_camaras = Camara.get_ordered_by_num_comments().count()
    n_coments = comentarios.count()

    fecha = timezone.now()

    if request.method == "POST":
        coment = request.POST.get('texto')
        if coment:
            img = capturar_imagen_de_camara(camara)
            comentario = Comentario(
                camara=camara,
                autor_nombre=configuracion.nombre,
                texto=coment,
                fecha=timezone.now(),
                imagen=img
            )
            comentario.save()
            return redirect(f'/camara/{id}/')
        else:
            return HttpResponse("Error, por favor introduce un comentario.", status=400)

    context = {
        'n_camaras': n_camaras,
        'n_coments': n_coments,
        'usuario': configuracion,
        'id': id,
        'camara': camara,
        'fecha': fecha
    }
    return render(request, 'teveoapp/comentario.html', context)


# Indica que la solicitud proviene de un navegador Firefox
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0'
}


def descargar_imagen(ident):
    """
    Descarga imagen desde una URL y la devuelve en forma de bytes
    """
    try:
        # Obtiene la instancia de Camara con el ID proporcionado en ident
        camara = Camara.objects.get(id=ident)
        # Obtiene la URL de la fuente de la cámara
        url_cam = camara.fuente
        # print(f"Descargando imagen desde {url_cam}") # -> error 403 en PythonAnyWhere
        # Define los parámetros para la solicitud HTTP
        request = urllib.request.Request(url=url_cam, headers=headers)
        # Abre la conexión y realiza la solicitud HTTP
        with urllib.request.urlopen(request) as response:
            # Leer la respuesta HTTP y guardarla como imagen en bytes
            image = response.read()
            # Devuelve la imagen descargada
            return image
            # print("Imagen descargada con éxito")

    # Captura la excepción si no se encuentra la Camara
    except Camara.DoesNotExist:
        print("No existe la cámara")
        return None

    # Captura la excepción si hay un error al realizar la solicitud HTTP
    except urllib.error.URLError:
        # print(f"Error al descargar la imagen: {e}")
        return None


def imagen():
    """
    Devuelve imagen descargada como una respuesta HTTP
    """
    # Obtener imagen en bytes
    image = descargar_imagen(id)
    if image is None:
        return HttpResponse("Error al descargar la imagen", status=500)
    else:
        # Respuesta HTTP con la imagen
        return HttpResponse(image, content_type="image/jpeg")


def image_embedded(request, camara_id):
    """
    Maneja la solicitud web y devuelve una respuesta HTML que contiene una imagen
    incrustada directamente en el HTML utilizando codificación en Base 64
    """
    # Llama a la función descargar_imagen() para obtener la imagen en bytes
    image = descargar_imagen(camara_id)

    if image is None:
        # Si no se pudo descargar la imagen, error con código 500
        return HttpResponse("Error al descargar la imagen", status=500)
    else:
        # Codifica la imagen en base64 para ser incrustada en el HTML
        image_base64 = base64.b64encode(image).decode('utf-8')

        # Crea el fragmento de HTML que incrusta la imagen en formato base64
        html = f'<img src="data:image/jpeg;base64,{image_base64}">'

        # Retorna una respuesta HTTP con el HTML que contiene la imagen incrustada
        return HttpResponse(html, content_type="text/html")


def comment_embedded(request, camara_id):
    # Obtener el objeto Camara dado su id (camara_id) o error 404 not found
    camara = get_object_or_404(Camara, id=camara_id)

    # Obtener todos los comentarios ORDENADOS para la cámara
    comentarios = Comentario.get_ordered_comments().filter(camara=camara)

    # Renderizar la plantilla con los comentarios obtenidos y devolver la respuesta HTTP
    return render(request, "teveoapp/comentariosdyn.html", {"comentarios": comentarios})


def configuracion(request):
    if not request.session.session_key:
        request.session.create()

    session_key = request.session.session_key
    configuracion = Usuario.objects.filter(id=session_key).first()

    if not configuracion:
        configuracion = Usuario(id=session_key)
        configuracion.save()

    comentarios = Comentario.get_ordered_comments()

    n_camaras = Camara.get_ordered_by_num_comments().count()
    n_coments = comentarios.count()

    opciones_formato = [formato[0] for formato in Usuario.FORMATO]
    opciones_tipo = [tipo[0] for tipo in Usuario.TIPO]

    authorize_url = request.session.get('authorize_url')

    if request.method == 'POST':
        if "nombre" in request.POST:
            nuevo_nombre = request.POST.get("nombre") or "Anonimo"
            configuracion.nombre = nuevo_nombre
            configuracion.save()

        if "formato" in request.POST:
            formato = request.POST.get("formato") or "Calibri"
            configuracion.formato = formato
            configuracion.save()

        if "tipo" in request.POST:
            tipo = request.POST.get("tipo") or "Medium"
            configuracion.tipo = tipo
            configuracion.save()

        if 'authorize_link' in request.POST:
            authorize_url = f"{request.build_absolute_uri()}?id_session={session_key}"
            request.session['authorize_url'] = authorize_url

        return redirect('configuracion')

    elif request.method == 'GET':
        session_id = request.GET.get('id_session')
        if session_id:
            try:
                authorized_settings = Usuario.objects.get(id=session_id)
                configuracion.nombre = authorized_settings.nombre
                configuracion.formato = authorized_settings.formato
                configuracion.tipo = authorized_settings.tipo
                configuracion.save()

                return redirect("configuracion")

            except (Usuario.DoesNotExist, ValueError):
                return HttpResponseNotFound("Error con el usuario")

    return render(request, 'teveoapp/configuracion.html', {
        "opciones_formato": opciones_formato,
        "opciones_tipo": opciones_tipo,
        "n_camaras": n_camaras,
        "n_coments": n_coments,
        "usuario": configuracion,
        "authorize_url": authorize_url
    })


def ayuda(request):
    """
    Manejador de vista ayuda
    """
    if not request.session.session_key:
        request.session.create()

    session_key = request.session.session_key
    configuracion, created = Usuario.objects.get_or_create(
        id=session_key,
        defaults={'nombre': 'Anonimo', 'formato': 'Calibri', 'tipo': 'Medium'}
    )

    comentarios = Comentario.get_ordered_comments()

    n_camaras = Camara.get_ordered_by_num_comments().count()
    n_coments = comentarios.count()

    context = {
        "n_camaras": n_camaras,
        "n_coments": n_coments,
        "configuracion": configuracion,
        "usuario": configuracion
    }
    return render(request, "teveoapp/ayuda.html", context)


def json(request, id):
    """
    Manejador de JSON para mostrar la información de una cámara específica en formato JSON
    """
    try:
        camara = Camara.objects.get(id=id)
    except Camara.DoesNotExist:
        return HttpResponseNotFound("La cámara solicitada no existe.")

    n_coments = Comentario.get_ordered_comments().filter(camara=camara).count()

    # Diccionario {} para JSON
    camara_info = {
        'id': camara.id,
        'ubicacion': camara.ubicacion,
        'latitud': camara.latitud,
        'longitud': camara.longitud,
        'fuente': camara.fuente,
        'numero_comentarios': n_coments
    }

    return JsonResponse(camara_info)


def salir(request):
    """
    Manejador de vista salir (resetear a valores por defecto)
    """
    if not request.session.session_key:
        request.session.create()

    session_key = request.session.session_key

    configuracion, created = Usuario.objects.get_or_create(
        id=session_key,
        defaults={'nombre': 'Anonimo', 'formato': 'Calibri', 'tipo': 'Medium'}
    )
    configuracion.nombre = 'Anonimo'
    configuracion.formato = 'Calibri'
    configuracion.tipo = 'Medium'
    configuracion.save()

    request.session.flush()

    return redirect('index')
