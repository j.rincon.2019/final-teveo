from django.db import models
from django.db.models import Count


class Usuario(models.Model):
    """
    Base de datos usada para guardar información acerca del nombre del visitante, tamaño y tipo de letra
    """
    id = models.CharField(max_length=80, primary_key=True)
    nombre = models.CharField(max_length=64, default="Anonimo")

    FORMATO = [
        ('Calibri', 'Calibri'),
        ('Times-New-Roman', 'Times New Roman'),
        ('Courier-New', 'Courier New'),
        ('Cursive', 'Cursive')
    ]
    formato = models.CharField(max_length=64, choices=FORMATO, default='Calibri')

    TIPO = [
        ('Large', 'Grande'),
        ('Medium', 'Mediana'),
        ('Small', 'Pequeña')
    ]
    tipo = models.CharField(max_length=64, choices=TIPO, default='Medium')


class Camara(models.Model):
    """
    Base de datos encargada de guardar la información sobre una cámara
    """
    id = models.CharField(max_length=80, primary_key=True)
    ubicacion = models.CharField(max_length=64)
    longitud = models.CharField(max_length=64)
    latitud = models.CharField(max_length=64)
    fuente = models.URLField()

    @staticmethod
    def get_ordered_by_num_comments():
        return Camara.objects.annotate(num_comentarios=Count('comentario')).order_by('-num_comentarios')


class Comentario(models.Model):
    """
    Base de datos encargada de guardar la información sobre un comentario, asociado a una cámara
    """
    camara = models.ForeignKey(Camara, on_delete=models.CASCADE)
    autor_nombre = models.CharField(max_length=64)
    texto = models.TextField(blank=False)
    fecha = models.DateTimeField()
    imagen = models.TextField(null=True, blank=True)  # Imagen en formato base64

    @staticmethod
    def get_ordered_comments():
        return Comentario.objects.order_by("-fecha")
